
var game = new Phaser.Game(800, 700, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('bullet_1', 'assets/bullet_1.png');
    ///game.load.image('bullet_1', 'assets/bullet_1.png');
    game.load.image('enemyBullet', 'assets/enemy-bullet.png');
    game.load.spritesheet('invader', 'assets/enemy.png', 48, 55);
    game.load.spritesheet('enemy', 'assets/enemy_L3.png', 55, 66);
    game.load.spritesheet('elite', 'assets/elite.png', 146, 200);
    game.load.image('ship', 'assets/player.png');
    game.load.spritesheet('aircraft', 'assets/aircraft.png', 64, 64);
    game.load.image('heart', 'assets/heart.png');
    game.load.image('elite_hp', 'assets/green_bar.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.image('starfield', 'assets/ocean.jpg');
    game.load.image('background', 'assets/background2.png');
    // audio
    game.load.audio('shot1', 'audio/shot1.wav');
    game.load.audio('shot2', 'audio/shot2.wav');
    game.load.audio('e_death', 'audio/explosion_e.mp3');
    game.load.audio('p_death', 'audio/player_death.wav');
    game.load.audio('sword', 'audio/sword.mp3');
    //
    game.load.image('diamond', 'assets/diamond.png');
}
var emitter;
var keys;///
var shot_1;///
var shot_2;///
var e_death;
var p_death;///
var sword;
var player;
var aliens;
var aliens_2;
var elite;
var bullets;
var bullets_1;///
var cursors;
var fireButton;
var fireButton_1;///
var explosions;
var explosions_1;///
var starfield;
var elite_lives;
var lives;
var scoreText;
var liveText;
var enemyBullets;
var enemyBullets_2;
var enemyBullets_3;
var enemyBullets_4;
var stateText;
var score = 0;
var firingTimer = 3000;
var bulletTime = 0;
var bulletTime_1 = 0;
var cur_live = 5;
var livingEnemies = [];
var scoreString = '';
var liveString = '';
var nextEnemyAt;
var enemyDelay;
var instruction;
var pauseing;
var instExpire = 0;
//
var skillText;
var skillString_1 = '';
var skill_state_1 = '';
var pauseText;
var slowTime = 0;
function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 700, 'starfield');
    // audio
    shot_1 = game.add.audio('shot1');///
    shot_2 = game.add.audio('shot2');///
    e_death = game.add.audio('e_death');///
    p_death = game.add.audio('p_death');
    sword = game.add.audio('sword');///
    game.sound.setDecodedCallback([shot_1, shot_2], start, this);///
    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    //bullets 2
    bullets_1 = game.add.group();
    bullets_1.enableBody = true;
    bullets_1.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_1.createMultiple(30, 'bullet_1');
    bullets_1.setAll('anchor.x', 0.5);
    bullets_1.setAll('anchor.y', 1);
    bullets_1.setAll('outOfBoundsKill', true);
    bullets_1.setAll('checkWorldBounds', true);
    ///
    bullets_2 = game.add.group();
    bullets_2.enableBody = true;
    bullets_2.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_2.createMultiple(30, 'bullet_1');
    bullets_2.setAll('anchor.x', 0.5);
    bullets_2.setAll('anchor.y', 1);
    bullets_2.setAll('outOfBoundsKill', true);
    bullets_2.setAll('checkWorldBounds', true);
    ///
    bullets_3 = game.add.group();
    bullets_3.enableBody = true;
    bullets_3.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_3.createMultiple(30, 'bullet_1');
    bullets_3.setAll('anchor.x', 0.5);
    bullets_3.setAll('anchor.y', 1);
    bullets_3.setAll('outOfBoundsKill', true);
    bullets_3.setAll('checkWorldBounds', true);
    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);

    enemyBullets_2 = game.add.group();
    enemyBullets_2.enableBody = true;
    enemyBullets_2.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_2.createMultiple(30, 'enemyBullet');
    enemyBullets_2.setAll('anchor.x', 0.5);
    enemyBullets_2.setAll('anchor.y', 1);
    enemyBullets_2.setAll('outOfBoundsKill', true);
    enemyBullets_2.setAll('checkWorldBounds', true);

    enemyBullets_3 = game.add.group();
    enemyBullets_3.enableBody = true;
    enemyBullets_3.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_3.createMultiple(30, 'enemyBullet');
    enemyBullets_3.setAll('anchor.x', 0.5);
    enemyBullets_3.setAll('anchor.y', 1);
    enemyBullets_3.setAll('outOfBoundsKill', true);
    enemyBullets_3.setAll('checkWorldBounds', true);

    enemyBullets_4 = game.add.group();
    enemyBullets_4.enableBody = true;
    enemyBullets_4.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_4.createMultiple(30, 'enemyBullet');
    enemyBullets_4.setAll('anchor.x', 0.5);
    enemyBullets_4.setAll('anchor.y', 1);
    enemyBullets_4.setAll('outOfBoundsKill', true);
    enemyBullets_4.setAll('checkWorldBounds', true);
    //  The hero!
    ///player = game.add.sprite(400, 500, 'ship');
    ///player.anchor.setTo(0.5, 0.5);
    ///game.physics.enable(player, Phaser.Physics.ARCADE);
    player = game.add.sprite(400, 500, 'aircraft');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.animations.add('fly');
    player.play('fly', 1000, true);
    // elite
    elite = game.add.group();
    elite.enableBody = true;
    elite.physicsBodyType = Phaser.Physics.ARCADE;
    ///game.physics.enable(elite, Phaser.Physics.ARCADE);
    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;
    ///
    aliens_2 = game.add.group();
    aliens_2.enableBody = true;
    aliens_2.physicsBodyType = Phaser.Physics.ARCADE;
    aliens_2.createMultiple(50, 'enemy');
    aliens_2.setAll('anchor.x', 0.5);
    aliens_2.setAll('anchor.y', 0.5);
    aliens_2.setAll('outOfBoundsKill', true);
    aliens_2.setAll('checkWorldBounds', true);
    nextEnemyAt = 0;///
    enemyDelay = 1000;///

    createAliens();
    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(game.world.width - 180, 10, scoreString + score, { font: '20px Arial', fill: '#FFF' });

    //  Lives
    liveString = 'Lives : ';
    liveText = game.add.text(10, 10, liveString, { font: '20px Arial', fill: '#FFF' });
    lives = game.add.group();
    //
    skillString_1 = 'Z : ';
    skill_state_1 = 'Ready!'
    skillText = game.add.text(10, 450, skillString_1 + skill_state_1, { font: '13px Arial', fill: '#FFF' });

    pauseText = game.add.text(10, 500, 'P : pause\nSPACE : normal attack', { font: '13px Arial', fill: '#FFF' });
    // elite lives
    elite_lives = game.add.group();
    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#FFF' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;
    //
    pauseing = game.add.text(400, 500, 'GAME PAUSE', {font: '30px monospace', fill: '#fff', align: 'center'});
    pauseing.anchor.setTo(0.5, 0.5);
    pauseing.visible = false;
    //
    //
    for(var k = 0; k < 2; k++){

        for (var i = 0; i < 5; i++) {
            var ship = lives.create(15 + (30 * i), 60 + (30 * k), 'heart');
            ship.anchor.setTo(0.5, 0.5);
            ///ship.angle = 90;
            ship.alpha = 0.8;
        }
    }
    for (var i = 0; i < 55; i++) ///
    {
        var monster = elite_lives.create(10 + (10 * i), 610, 'elite_hp');
        monster.anchor.setTo(0.5, 0.5);
        ///ship.angle = 90;
        monster.alpha = 1;
    }
    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    fireButton_1 = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    window.onkeydown = function(event) {
        
        if (event.keyCode == 80){
            game.paused = !game.paused;
        }
        if(game.paused === false){

            stateText.visible = false;
        }
        else{

            stateText.text=" PAUSE \n Press P To Resume";
            stateText.visible = true;
        }
    }
    emitter = game.add.emitter(0, 0, 100);

    emitter.makeParticles('diamond');
    emitter.gravity = 500;
}
function start(){///

    ///shot_1.onStop.add(soundStopped, this);
    keys = game.input.keyboard.addKeys({shot_1 : Phaser.Keyboard.SPACEBAR, shot_2 : Phaser.Keyboard.Z});
    keys.shot_1.onDown.add(playFx, this);
    keys.shot_2.onDown.add(playFx, this);
}

function playFx(key){///

    switch(key.keyCode){

        case Phaser.Keyboard.SPACEBAR : 
            shot_1.play();
            break;
        case Phaser.Keyboard.Z : 
            shot_2.play();
            break;
    }
}
function createAliens () {

    var elite_m = elite.create(3 * 90, 2 * 25, 'elite');
    elite_m.anchor.setTo(0.5, 0.5);
    ///alien.animations.add('fly', [ 0, 1, 2, 3 ], 20, true);
    ///alien.play('fly');
    elite_m.body.moves = false;
    elite.x = 100;
    elite.y = 50;
    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(elite).to( { x: 400 }, 1200, Phaser.Easing.Linear.None, true, 0, 1000, true);
    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
    
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function update() {

    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);
        if (cursors.left.isDown)
        {
            if(game.time.now > slowTime){

                player.body.velocity.x = -250;
            }
            else{

                player.body.velocity.x = -100;
            }
            
        }
        else if (cursors.right.isDown)
        {
            if(game.time.now > slowTime){

                player.body.velocity.x = 250;
            }
            else{

                player.body.velocity.x = 100;
            }
        }
        else if(cursors.up.isDown){

            if(game.time.now > slowTime){

                player.body.velocity.y = -250;
            }
            else{

                player.body.velocity.y = -100;
            }
        }
        else if(cursors.down.isDown){

            if(game.time.now > slowTime){

                player.body.velocity.y = 250;
            }
            else{

                player.body.velocity.y = 100;
            }
        }
        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }
        else if(fireButton_1.isDown){

            fireBullet_1();
            instExpire = game.time.now + 7000;
        }
        ///
        if (game.time.now > firingTimer)
        {
            
            suicideFires();
            enemyFires();
        }
        if(game.time.now >= bulletTime_1){

            skill_state_1 = 'Ready!';
            skillText.text = skillString_1 + skill_state_1;
        }
        //  Run collision
        ///game.physics.arcade.overlap(bullets, aliens, collisionHandler, null, this);
        game.physics.arcade.overlap(bullets, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets, elite, playerHitelite, null, this);
        ///game.physics.arcade.overlap(bullets_1, aliens, collisionHandler_1, null, this);
        game.physics.arcade.overlap(bullets_1, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_1, elite, playerHitelite_1, null, this);
        game.physics.arcade.overlap(bullets_2, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_2, elite, playerHitelite_1, null, this);
        game.physics.arcade.overlap(bullets_3, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_3, elite, playerHitelite_1, null, this);
        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_2, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_3, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_4, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(aliens_2, player, playerHit, null, this);

    }

}

function render() {

    // for (var i = 0; i < aliens.length; i++)
    // {
    //     game.debug.body(aliens.children[i]);
    // }

}

function flyenemyHandler (bullet, alien) {

    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();
    //audio
    e_death.play();
    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;
    //
    emitter.x = alien.body.x;
    emitter.y = alien.body.y;
    emitter.start(true, 2000, null, 10);
    //  And create an explosion :)
    //var explosion = explosions.getFirstExists(false);
    //explosion.reset(alien.body.x, alien.body.y);
    //explosion.play('kaboom', 30, false, true);

    if (elite_lives.countLiving() == 0)
    {
        score += 2200;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        aliens_2.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}
function playerHitelite(bullet, elite){

    //audio
    e_death.play();
    //
    bullet.kill();
    score += 20;
    scoreText.text = scoreString + score;
    elite_live = elite_lives.getFirstAlive();
    if(elite_live){

        elite_live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(elite.body.x + 74, elite.body.y + 103);
    explosion.play('kaboom', 30, false, true);
    if (elite_lives.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 2200;
        scoreText.text = scoreString + score;
        elite.kill();
        enemyBullets.callAll('kill');
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        aliens_2.callAll('kill');

        stateText.text=" YOU WIN!!! \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }
}
function playerHitelite_1(bullet, elite){

    //audio
    e_death.play();
    //
    bullet.kill();
    score += 20;
    scoreText.text = scoreString + score;
    elite_live = elite_lives.getFirstAlive();
    if(elite_live){

        elite_live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(elite.body.x + 74, elite.body.y + 103);
    explosion.play('kaboom', 30, false, true);
    if (elite_lives.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 2200;
        scoreText.text = scoreString + score;
        elite.kill();
        enemyBullets.callAll('kill');
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        aliens_2.callAll('kill');

        stateText.text=" YOU WIN!!! \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }
}
function enemyHitsPlayer (player,bullet) {
    
    //audio
    e_death.play();
    //
    bullet.kill();
    cur_live -= 1;
    ///liveText.text = liveString + cur_live;
    liveText.text = liveString;
    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x + 24, player.body.y + 42);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        ///cur_live = 5;
        player.kill();
        enemyBullets.callAll('kill');
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        aliens_2.callAll('kill');

        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}
function playerHit(player, enemy) { 
    
    //audio
    p_death.play();
    //
    enemy.kill();
    slowTime = game.time.now + 2500;
    cur_live -= 1;
    ///liveText.text = liveString + cur_live;
    liveText.text = liveString;
    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }
    //
    emitter.x = enemy.body.x;
    emitter.y = enemy.body.y;
    emitter.start(true, 2000, null, 10);
    //  And create an explosion :)
    //var explosion = explosions.getFirstExists(false);
    //explosion.reset(player.body.x + 24, player.body.y + 42);
    //explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        ///cur_live = 5;
        player.kill();
        enemyBullets.callAll('kill');
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        aliens_2.callAll('kill');

        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }
}
function enemyFires () {

    //audio
    sword.play();
    //  Grab the first bullet we can from the pool
    enemyBullet_a = enemyBullets.getFirstExists(false);
    enemyBullet_b = enemyBullets_2.getFirstExists(false);
    enemyBullet_c = enemyBullets_3.getFirstExists(false);
    enemyBullet_d = enemyBullets_4.getFirstExists(false);
    livingEnemies.length=0;

    elite.forEachAlive(function(elite_m){

        // put every living enemy in an array
        livingEnemies.push(elite_m);
    });


    if (enemyBullet_a && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet_a.reset(shooter.body.x, shooter.body.y);
        enemyBullet_b.reset(shooter.body.x, shooter.body.y);
        enemyBullet_c.reset(shooter.body.x, shooter.body.y);
        enemyBullet_d.reset(shooter.body.x, shooter.body.y);
        game.physics.arcade.moveToObject(enemyBullet_a,player,700);
        game.physics.arcade.moveToObject(enemyBullet_b,player,500);
        enemyBullet_c.body.velocity.y = 500;
        enemyBullet_c.body.velocity.x = -250;
        enemyBullet_d.body.velocity.y = 500;
        enemyBullet_d.body.velocity.x = 250;
        //game.physics.arcade.moveToObject(enemyBullet_c,player,350);
        firingTimer = game.time.now + 3000;
    }

}
function suicideFires(){

    suicideEnemy = aliens_2.getFirstExists(false);
    livingEnemies.length=0;

    elite.forEachAlive(function(elite_m){

        // put every living enemy in an array
        livingEnemies.push(elite_m);
    });
    if (suicideEnemy && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        suicideEnemy.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(suicideEnemy,player,75);
        firingTimer = game.time.now + 3000;
    }
}
function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 700;
        }
    }

}
function fireBullet_1(){

    if (game.time.now > bulletTime_1)
    {
        //  Grab the first bullet we can from the pool
        skill_state_1 = 'Reloading';
        skillText.text = skillString_1 + skill_state_1;
        bullet_a = bullets_1.getFirstExists(false);
        bullet_b = bullets_2.getFirstExists(false);
        bullet_c = bullets_3.getFirstExists(false);
        if (bullet_a)
        {
            //  And fire it
            bullet_a.reset(player.x, player.y + 12);
            bullet_a.body.velocity.y = -600;
            bullet_b.reset(player.x, player.y + 10);
            bullet_b.body.velocity.y = -450;
            bullet_c.reset(player.x, player.y + 8);
            bullet_c.body.velocity.y = -300;
            bulletTime_1 = game.time.now + 8000;
        }
    }
}
/*function pauseAct(){

    if(game.paused == true){

        game.paused == false;
        pauseing.visible = false;
    }
    else{

        game.paused == true;
        pauseing.visible = false;
    }
}*/
function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function restart () {

    //  A new level starts
    /*cur_live = 5;
    //resets the life count
    lives.callAll('revive');
    elite_lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    
    createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
    score = 0;
    scoreText.text = scoreString + score;*/
    create();
}
