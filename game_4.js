
var game = new Phaser.Game(800, 700, Phaser.AUTO, 'phaser-example', { preload: preload, create: create, update: update, render: render });

function preload() {

    game.load.image('bullet', 'assets/bullet.png');
    game.load.image('bullet_1', 'assets/bullet_1.png');
    game.load.image('bullet_2', 'assets/bullet_2.png');
    game.load.image('enemyBullet', 'assets/enemy-bullet.png');
    game.load.image('enemyBullet_1', 'assets/enemy-bullet_1.png');
    game.load.spritesheet('invader', 'assets/enemy.png', 48, 55);
    game.load.spritesheet('enemy', 'assets/enemy_LL3.png', 48, 48);
    game.load.spritesheet('enemy_1', 'assets/enemy_LL4.png', 48, 48);
    game.load.spritesheet('elite', 'assets/queen.png', 128, 204);
    game.load.spritesheet('guard', 'assets/guard.png', 88, 146);
    game.load.image('ship', 'assets/player.png');
    game.load.spritesheet('aircraft', 'assets/aircraft.png', 64, 64);
    game.load.image('heart', 'assets/heart.png');
    game.load.image('elite_hp', 'assets/green_bar.png');
    game.load.image('guard_hp', 'assets/red_bar.png');
    game.load.spritesheet('kaboom', 'assets/explode.png', 128, 128);
    game.load.image('starfield', 'assets/starfield.jpg');
    game.load.image('background', 'assets/background2.png');
    // audio
    game.load.audio('shot1', 'audio/shot1.wav');
    game.load.audio('shot2', 'audio/shot2.wav');
    game.load.audio('shot3', 'audio/shotgun.wav');
    game.load.audio('e_death', 'audio/explosion_e.mp3');
    game.load.audio('p_death', 'audio/player_death.wav');
    game.load.audio('sword', 'audio/sword.mp3');
    //
    game.load.image('diamond', 'assets/diamond.png');
}
var emitter;
var keys;///
var shot_1;///
var shot_2;///
var shot_3;///
var e_death;
var p_death;///
var sword;
var player;
var aliens;
var aliens_2;
var aliens_3;
var guards_1;
var guards_2;
var guards_3;
var elite;
var bullets;
var bullets_1;
var bullets_2;
var bullets_3;
var bullets_4;
var bullets_5;
var bullets_6;///
var cursors;
var fireButton;
var fireButton_1;///
var explosions;
var explosions_1;///
var starfield;
var elite_lives;
var guards_1_lives;
var guards_2_lives;
var guards_3_lives;
var lives;
var scoreText;
var liveText;
var enemyBullets;
var enemyBullets_1;
var enemyBullets_2;
var enemyBullets_3;
var enemyBullets_4;
var enemyBullets_5;
var stateText;
var score = 0;
var firingTimer = 3000;
var firingTimer_1 = 5000;
var firingTimer_2 = 0;
var firingTimer_3 = 0;
var bulletTime = 0;
var bulletTime_1 = 0;
var bulletTime_2 = 0;
var cur_live = 5;
var livingEnemies = [];
var scoreString = '';
var liveString = '';
var instruction;
var pauseing;
var instExpire = 0;
var skillText;
var skillString_1 = '';
var skill_state_1 = '';
var skillText_0;
var skillString_2 = '';
var skill_state_2 = '';
var pauseText;
var slowTime = 0;
function create() {

    game.physics.startSystem(Phaser.Physics.ARCADE);

    //  The scrolling starfield background
    starfield = game.add.tileSprite(0, 0, 800, 700, 'starfield');
    // audio
    shot_1 = game.add.audio('shot1');///
    shot_2 = game.add.audio('shot2');///
    shot_3 = game.add.audio('shot3');///
    e_death = game.add.audio('e_death');///
    p_death = game.add.audio('p_death');
    sword = game.add.audio('sword');///
    game.sound.setDecodedCallback([shot_1, shot_2, shot_3], start, this);///
    //  Our bullet group
    bullets = game.add.group();
    bullets.enableBody = true;
    bullets.physicsBodyType = Phaser.Physics.ARCADE;
    bullets.createMultiple(30, 'bullet');
    bullets.setAll('anchor.x', 0.5);
    bullets.setAll('anchor.y', 1);
    bullets.setAll('outOfBoundsKill', true);
    bullets.setAll('checkWorldBounds', true);
    //bullet 1
    bullets_1 = game.add.group();
    bullets_1.enableBody = true;
    bullets_1.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_1.createMultiple(30, 'bullet_1');
    bullets_1.setAll('anchor.x', 0.5);
    bullets_1.setAll('anchor.y', 1);
    bullets_1.setAll('outOfBoundsKill', true);
    bullets_1.setAll('checkWorldBounds', true);
    ///
    bullets_2 = game.add.group();
    bullets_2.enableBody = true;
    bullets_2.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_2.createMultiple(30, 'bullet_1');
    bullets_2.setAll('anchor.x', 0.5);
    bullets_2.setAll('anchor.y', 1);
    bullets_2.setAll('outOfBoundsKill', true);
    bullets_2.setAll('checkWorldBounds', true);
    ///
    bullets_3 = game.add.group();
    bullets_3.enableBody = true;
    bullets_3.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_3.createMultiple(30, 'bullet_1');
    bullets_3.setAll('anchor.x', 0.5);
    bullets_3.setAll('anchor.y', 1);
    bullets_3.setAll('outOfBoundsKill', true);
    bullets_3.setAll('checkWorldBounds', true);
    //
    //bullet 2
    bullets_4 = game.add.group();
    bullets_4.enableBody = true;
    bullets_4.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_4.createMultiple(30, 'bullet_2');
    bullets_4.setAll('anchor.x', 0.5);
    bullets_4.setAll('anchor.y', 1);
    bullets_4.setAll('outOfBoundsKill', true);
    bullets_4.setAll('checkWorldBounds', true);
    ///
    bullets_5 = game.add.group();
    bullets_5.enableBody = true;
    bullets_5.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_5.createMultiple(30, 'bullet_2');
    bullets_5.setAll('anchor.x', 0.5);
    bullets_5.setAll('anchor.y', 1);
    bullets_5.setAll('outOfBoundsKill', true);
    bullets_5.setAll('checkWorldBounds', true);
    ///
    bullets_6 = game.add.group();
    bullets_6.enableBody = true;
    bullets_6.physicsBodyType = Phaser.Physics.ARCADE;
    bullets_6.createMultiple(30, 'bullet_2');
    bullets_6.setAll('anchor.x', 0.5);
    bullets_6.setAll('anchor.y', 1);
    bullets_6.setAll('outOfBoundsKill', true);
    bullets_6.setAll('checkWorldBounds', true);
    // The enemy's bullets
    enemyBullets = game.add.group();
    enemyBullets.enableBody = true;
    enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets.createMultiple(30, 'enemyBullet');
    enemyBullets.setAll('anchor.x', 0.5);
    enemyBullets.setAll('anchor.y', 1);
    enemyBullets.setAll('outOfBoundsKill', true);
    enemyBullets.setAll('checkWorldBounds', true);
    // The enemy's bullets_1
    enemyBullets_1 = game.add.group();
    enemyBullets_1.enableBody = true;
    enemyBullets_1.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_1.createMultiple(30, 'enemyBullet_1');
    enemyBullets_1.setAll('anchor.x', 0.5);
    enemyBullets_1.setAll('anchor.y', 1);
    enemyBullets_1.setAll('outOfBoundsKill', true);
    enemyBullets_1.setAll('checkWorldBounds', true);

    enemyBullets_2 = game.add.group();
    enemyBullets_2.enableBody = true;
    enemyBullets_2.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_2.createMultiple(30, 'enemyBullet_1');
    enemyBullets_2.setAll('anchor.x', 0.5);
    enemyBullets_2.setAll('anchor.y', 1);
    enemyBullets_2.setAll('outOfBoundsKill', true);
    enemyBullets_2.setAll('checkWorldBounds', true);

    enemyBullets_3 = game.add.group();
    enemyBullets_3.enableBody = true;
    enemyBullets_3.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_3.createMultiple(30, 'enemyBullet');
    enemyBullets_3.setAll('anchor.x', 0.5);
    enemyBullets_3.setAll('anchor.y', 1);
    enemyBullets_3.setAll('outOfBoundsKill', true);
    enemyBullets_3.setAll('checkWorldBounds', true);

    enemyBullets_4 = game.add.group();
    enemyBullets_4.enableBody = true;
    enemyBullets_4.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_4.createMultiple(30, 'enemyBullet');
    enemyBullets_4.setAll('anchor.x', 0.5);
    enemyBullets_4.setAll('anchor.y', 1);
    enemyBullets_4.setAll('outOfBoundsKill', true);
    enemyBullets_4.setAll('checkWorldBounds', true);

    enemyBullets_5 = game.add.group();
    enemyBullets_5.enableBody = true;
    enemyBullets_5.physicsBodyType = Phaser.Physics.ARCADE;
    enemyBullets_5.createMultiple(30, 'enemyBullet');
    enemyBullets_5.setAll('anchor.x', 0.5);
    enemyBullets_5.setAll('anchor.y', 1);
    enemyBullets_5.setAll('outOfBoundsKill', true);
    enemyBullets_5.setAll('checkWorldBounds', true);
    //  The hero!
    ///player = game.add.sprite(400, 500, 'ship');
    ///player.anchor.setTo(0.5, 0.5);
    ///game.physics.enable(player, Phaser.Physics.ARCADE);
    player = game.add.sprite(400, 500, 'aircraft');
    player.anchor.setTo(0.5, 0.5);
    game.physics.enable(player, Phaser.Physics.ARCADE);
    player.animations.add('fly');
    player.play('fly', 1000, true);
    // elite
    elite = game.add.group();
    elite.enableBody = true;
    elite.physicsBodyType = Phaser.Physics.ARCADE;
    // guards
    guards_1 = game.add.group();
    guards_1.enableBody = true;
    guards_1.physicsBodyType = Phaser.Physics.ARCADE;
    guards_2 = game.add.group();
    guards_2.enableBody = true;
    guards_2.physicsBodyType = Phaser.Physics.ARCADE;
    guards_3 = game.add.group();
    guards_3.enableBody = true;
    guards_3.physicsBodyType = Phaser.Physics.ARCADE;
    ///game.physics.enable(elite, Phaser.Physics.ARCADE);
    //  The baddies!
    aliens = game.add.group();
    aliens.enableBody = true;
    aliens.physicsBodyType = Phaser.Physics.ARCADE;
    ///
    aliens_2 = game.add.group();
    aliens_2.enableBody = true;
    aliens_2.physicsBodyType = Phaser.Physics.ARCADE;
    aliens_2.createMultiple(50, 'enemy');
    aliens_2.setAll('anchor.x', 0.5);
    aliens_2.setAll('anchor.y', 0.5);
    aliens_2.setAll('outOfBoundsKill', true);
    aliens_2.setAll('checkWorldBounds', true);
    //
    aliens_3 = game.add.group();
    aliens_3.enableBody = true;
    aliens_3.physicsBodyType = Phaser.Physics.ARCADE;
    aliens_3.createMultiple(50, 'enemy_1');
    aliens_3.setAll('anchor.x', 0.5);
    aliens_3.setAll('anchor.y', 0.5);
    aliens_3.setAll('outOfBoundsKill', true);
    aliens_3.setAll('checkWorldBounds', true);
    

    createAliens();
    //  The score
    scoreString = 'Score : ';
    scoreText = game.add.text(game.world.width - 180, 10, scoreString + score, { font: '20px Arial', fill: '#black' });

    //  Lives
    liveString = 'Lives : ';
    liveText = game.add.text(10, 10, liveString, { font: '20px Arial', fill: '#black' });
    lives = game.add.group();
    ///
    skillString_1 = 'Z : ';
    skill_state_1 = 'Ready!';
    skillText = game.add.text(10, 450, skillString_1 + skill_state_1, { font: '13px Arial', fill: '#black' });
    skillString_2 = 'X : ';
    skill_state_2 = 'Ready!';
    skillText_0 = game.add.text(10, 475, skillString_2 + skill_state_2, { font: '13px Arial', fill: '#black' });
    
    pauseText = game.add.text(10, 500, 'P : pause\nSPACE : normal attack', { font: '13px Arial', fill: '#black' });
    // elite lives
    elite_lives = game.add.group();
    // guards lives
    guards_1_lives = game.add.group();
    guards_2_lives = game.add.group();
    guards_3_lives = game.add.group();
    //  Text
    stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#black' });
    stateText.anchor.setTo(0.5, 0.5);
    stateText.visible = false;
    //
    pauseing = game.add.text(400, 500, 'GAME PAUSE', {font: '30px monospace', fill: '#fff', align: 'center'});
    pauseing.anchor.setTo(0.5, 0.5);
    pauseing.visible = false;
    //
    for(var k = 0; k < 2; k++){

        for (var i = 0; i < 5; i++) {
            var ship = lives.create(15 + (30 * i), 60 + (30 * k), 'heart');
            ship.anchor.setTo(0.5, 0.5);
            ///ship.angle = 90;
            ship.alpha = 0.8;
        }
    }
    
    for (var i = 0; i < 60; i++) ///
    {
        var monster = elite_lives.create(10 + (10 * i), 610, 'elite_hp');
        monster.anchor.setTo(0.5, 0.5);
        ///ship.angle = 90;
        monster.alpha = 1;
    }
    for(var i = 0; i < 15; i++){

        var insect = guards_1_lives.create(10 + (13 * i), 630, 'guard_hp');
        insect.anchor.setTo(0.5, 0.5);
        insect.alpha = 1;
    }
    for(var i = 0; i < 15; i++){

        var insect = guards_2_lives.create(10 + (13 * i), 650, 'guard_hp');
        insect.anchor.setTo(0.5, 0.5);
        insect.alpha = 1;
    }
    for(var i = 0; i < 15; i++){

        var insect = guards_3_lives.create(10 + (13 * i), 670, 'guard_hp');
        insect.anchor.setTo(0.5, 0.5);
        insect.alpha = 1;
    }
    //  An explosion pool
    explosions = game.add.group();
    explosions.createMultiple(30, 'kaboom');
    explosions.forEach(setupInvader, this);
    cursors = game.input.keyboard.createCursorKeys();
    fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
    fireButton_1 = game.input.keyboard.addKey(Phaser.Keyboard.Z);
    fireButton_2 = game.input.keyboard.addKey(Phaser.Keyboard.X);
    window.onkeydown = function(event) {
        
        if (event.keyCode == 80){
            game.paused = !game.paused;
        }
        if(game.paused === false){

            stateText.visible = false;
        }
        else{

            stateText.text=" PAUSE \n Press P To Resume";
            stateText.visible = true;
        }
    }
    emitter = game.add.emitter(0, 0, 100);

    emitter.makeParticles('diamond');
    emitter.gravity = 500;
}
function start(){///

    ///shot_1.onStop.add(soundStopped, this);
    keys = game.input.keyboard.addKeys({shot_1 : Phaser.Keyboard.SPACEBAR, shot_2 : Phaser.Keyboard.Z, shot_3 : Phaser.Keyboard.X});
    keys.shot_1.onDown.add(playFx, this);
    keys.shot_2.onDown.add(playFx, this);
    keys.shot_3.onDown.add(playFx, this);
}

function playFx(key){///

    switch(key.keyCode){

        case Phaser.Keyboard.SPACEBAR : 
            shot_1.play();
            break;
        case Phaser.Keyboard.Z : 
            shot_2.play();
            break;
        case Phaser.Keyboard.X : 
            shot_3.play();
            break;
    }
}
function createAliens () {

    var elite_m = elite.create(3 * 90, 2 * 25, 'elite');
    elite_m.anchor.setTo(0.5, 0.5);
    var guard_i1 = guards_1.create(200, 160, 'guard');
    guard_i1.anchor.setTo(0.5, 0.5);
    var guard_i2 = guards_2.create(270, 160, 'guard');
    guard_i2.anchor.setTo(0.5, 0.5);
    var guard_i3 = guards_3.create(340, 160, 'guard');
    guard_i3.anchor.setTo(0.5, 0.5);
    //
    elite_m.body.moves = false;
    elite.x = 100;
    elite.y = 50;
    guard_i1.body.moves = false;
    guards_1.x = 100;
    guards_1.y = 50;
    guard_i2.body.moves = false;
    guards_2.x = 100;
    guards_2.y = 50;
    guard_i3.body.moves = false;
    guards_3.x = 100;
    guards_3.y = 50;
    //  All this does is basically start the invaders moving. Notice we're moving the Group they belong to, rather than the invaders directly.
    var tween = game.add.tween(elite).to( { x: 400 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    var tween_1 = game.add.tween(guards_1).to( { x: 400 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    var tween_2 = game.add.tween(guards_2).to( { x: 400 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    var tween_3 = game.add.tween(guards_3).to( { x: 400 }, 2000, Phaser.Easing.Linear.None, true, 0, 1000, true);
    //  When the tween loops it calls descend
    tween.onLoop.add(descend, this);
    tween_1.onLoop.add(descend, this);
    tween_2.onLoop.add(descend, this);
    tween_3.onLoop.add(descend, this);
}

function setupInvader (invader) {

    invader.anchor.x = 0.5;
    invader.anchor.y = 0.5;
    invader.animations.add('kaboom');

}

function descend() {

    aliens.y += 10;

}

function update() {

    //  Scroll the background
    starfield.tilePosition.y += 2;

    if (player.alive)
    {
        //  Reset the player, then check for movement keys
        player.body.velocity.setTo(0, 0);
        if (cursors.left.isDown)
        {
            if(game.time.now > slowTime){

                player.body.velocity.x = -250;
            }
            else{

                player.body.velocity.x = -100;
            }
            
        }
        else if (cursors.right.isDown)
        {
            if(game.time.now > slowTime){

                player.body.velocity.x = 250;
            }
            else{

                player.body.velocity.x = 100;
            }
        }
        else if(cursors.up.isDown){

            if(game.time.now > slowTime){

                player.body.velocity.y = -250;
            }
            else{

                player.body.velocity.y = -100;
            }
        }
        else if(cursors.down.isDown){

            if(game.time.now > slowTime){

                player.body.velocity.y = 250;
            }
            else{

                player.body.velocity.y = 100;
            }
        }
        //  Firing?
        if (fireButton.isDown)
        {
            fireBullet();
        }
        else if(fireButton_1.isDown){

            fireBullet_1();
            ///instExpire = game.time.now + 7000;
        }
        else if(fireButton_2.isDown){

            fireBullet_2();
        }
        /*else if(pauseButton.isDown){

            pauseAct();
        }*/
        ///
        if (game.time.now > firingTimer_3)
        {
            
            suicideFires_1();
        }
        else if(game.time.now > firingTimer_1){

            enemyFires_1();
        }
        else if(game.time.now > firingTimer_2){

            suicideFires();
        }
        else if(game.time.now > firingTimer){

            enemyFires();
        }
        if(game.time.now >= bulletTime_1){

            skill_state_1 = 'Ready!';
            skillText.text = skillString_1 + skill_state_1;
        }
        if(game.time.now >= bulletTime_2){

            skill_state_2 = 'Ready!';
            skillText_0.text = skillString_2 + skill_state_2;
        }
        //  Run collision
        game.physics.arcade.overlap(bullets, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets, guards_3, playerHitguard_3, null, this);
        //
        game.physics.arcade.overlap(bullets_1, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_1, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_1, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets_1, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets_1, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets_1, guards_3, playerHitguard_3, null, this);

        game.physics.arcade.overlap(bullets_2, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_2, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_2, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets_2, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets_2, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets_2, guards_3, playerHitguard_3, null, this);

        game.physics.arcade.overlap(bullets_3, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_3, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_3, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets_3, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets_3, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets_3, guards_3, playerHitguard_3, null, this);

        game.physics.arcade.overlap(bullets_4, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_4, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_4, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets_4, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets_4, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets_4, guards_3, playerHitguard_3, null, this);
        ///
        game.physics.arcade.overlap(bullets_5, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_5, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_5, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets_5, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets_5, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets_5, guards_3, playerHitguard_3, null, this);

        game.physics.arcade.overlap(bullets_6, aliens_2, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_6, aliens_3, flyenemyHandler, null, this);
        game.physics.arcade.overlap(bullets_6, elite, playerHitelite, null, this);
        game.physics.arcade.overlap(bullets_6, guards_1, playerHitguard_1, null, this);
        game.physics.arcade.overlap(bullets_6, guards_2, playerHitguard_2, null, this);
        game.physics.arcade.overlap(bullets_6, guards_3, playerHitguard_3, null, this);

        game.physics.arcade.overlap(enemyBullets, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_1, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_2, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_3, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_4, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(enemyBullets_5, player, enemyHitsPlayer, null, this);
        game.physics.arcade.overlap(aliens_2, player, playerHit, null, this);
        game.physics.arcade.overlap(aliens_3, player, playerHit, null, this);
    }

}

function render() {

    // for (var i = 0; i < aliens.length; i++)
    // {
    //     game.debug.body(aliens.children[i]);
    // }

}

function flyenemyHandler (bullet, alien) {

    //audio
    e_death.play();
    //  When a bullet hits an alien we kill them both
    bullet.kill();
    alien.kill();

    //  Increase the score
    score += 20;
    scoreText.text = scoreString + score;
    //
    emitter.x = alien.body.x;
    emitter.y = alien.body.y;
    emitter.start(true, 2000, null, 10);
    //  And create an explosion :)
    //var explosion = explosions.getFirstExists(false);
    //explosion.reset(alien.body.x, alien.body.y);
    //explosion.play('kaboom', 30, false, true);

    if (elite_lives.countLiving() == 0)
    {
        score += 2400;
        scoreText.text = scoreString + score;

        enemyBullets.callAll('kill',this);
        enemyBullets_1.callAll('kill',this);
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        enemyBullets_5.callAll('kill',this);
        aliens_2.callAll('kill',this);
        aliens_3.callAll('kill',this);
        stateText.text = " You Won, \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}
function playerHitelite(bullet, elite){

    //audio
    e_death.play();
    //
    bullet.kill();
    elite_live = elite_lives.getFirstAlive();
    score += 20;
    scoreText.text = scoreString + score;
    if(elite_live){

        elite_live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(elite.body.x + 64, elite.body.y + 100);
    explosion.play('kaboom', 30, false, true);
    if (elite_lives.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 2400;
        scoreText.text = scoreString + score;
        elite.kill();
        enemyBullets.callAll('kill');
        enemyBullets_1.callAll('kill',this);
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        enemyBullets_5.callAll('kill',this);
        aliens_2.callAll('kill');
        aliens_3.callAll('kill',this);
        stateText.text=" YOU WIN!!! \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }
}
function playerHitguard_1(bullet, guard){

    //audio
    e_death.play();
    //
    bullet.kill();
    guards_1_live = guards_1_lives.getFirstAlive();
    score += 20;
    scoreText.text = scoreString + score;
    if(guards_1_live){

        guards_1_live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(guard.body.x + 44, guard.body.y + 73);
    explosion.play('kaboom', 30, false, true);
    if (guards_1_lives.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 500;
        scoreText.text = scoreString + score;
        guard.kill();
    }
}
function playerHitguard_2(bullet, guard){

    //audio
    e_death.play();
    //
    bullet.kill();
    guards_2_live = guards_2_lives.getFirstAlive();
    score += 20;
    scoreText.text = scoreString + score;
    if(guards_2_live){

        guards_2_live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(guard.body.x + 44, guard.body.y + 73);
    explosion.play('kaboom', 30, false, true);
    if (guards_2_lives.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 500;
        scoreText.text = scoreString + score;
        guard.kill();
    }
}
function playerHitguard_3(bullet, guard){

    //audio
    e_death.play();
    //
    bullet.kill();
    guards_3_live = guards_3_lives.getFirstAlive();
    score += 20;
    scoreText.text = scoreString + score;
    if(guards_3_live){

        guards_3_live.kill();
    }
    var explosion = explosions.getFirstExists(false);
    explosion.reset(guard.body.x + 44, guard.body.y + 73);
    explosion.play('kaboom', 30, false, true);
    if (guards_3_lives.countLiving() < 1)
    {
        ///cur_live = 5;
        score += 500;
        scoreText.text = scoreString + score;
        guard.kill();
    }
}
function enemyHitsPlayer (player,bullet) {
    
    //audio
    e_death.play();
    //
    bullet.kill();

    cur_live -= 1;
    ///liveText.text = liveString + cur_live;
    liveText.text = liveString;
    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }

    //  And create an explosion :)
    var explosion = explosions.getFirstExists(false);
    explosion.reset(player.body.x + 24, player.body.y + 42);
    explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        ///cur_live = 5;
        player.kill();
        enemyBullets.callAll('kill');
        enemyBullets_1.callAll('kill',this);
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        enemyBullets_5.callAll('kill',this);
        aliens_2.callAll('kill');
        aliens_3.callAll('kill',this);
        stateText.text=" GAME OVER QQ \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }

}
function playerHit(player, enemy) { 
    
    //audio
    p_death.play();
    //
    enemy.kill();
    slowTime = game.time.now + 2500;
    cur_live -= 1;
    ///liveText.text = liveString + cur_live;
    liveText.text = liveString;
    live = lives.getFirstAlive();

    if (live)
    {
        live.kill();
    }
    //
    emitter.x = enemy.body.x;
    emitter.y = enemy.body.y;
    emitter.start(true, 2000, null, 10);
    //  And create an explosion :)
    //var explosion = explosions.getFirstExists(false);
    //explosion.reset(player.body.x + 24, player.body.y + 42);
    //explosion.play('kaboom', 30, false, true);

    // When the player dies
    if (lives.countLiving() < 1)
    {
        ///cur_live = 5;
        player.kill();
        enemyBullets.callAll('kill');
        enemyBullets_1.callAll('kill',this);
        enemyBullets_2.callAll('kill',this);
        enemyBullets_3.callAll('kill',this);
        enemyBullets_4.callAll('kill',this);
        enemyBullets_5.callAll('kill',this);
        aliens_2.callAll('kill');
        aliens_3.callAll('kill',this);
        stateText.text=" GAME OVER \n Click to restart";
        stateText.visible = true;

        //the "click to restart" handler
        game.input.onTap.addOnce(restart,this);
    }
}
function enemyFires () {

    //audio
    sword.play();
    //  Grab the first bullet we can from the pool
    enemyBullet_a = enemyBullets_3.getFirstExists(false);
    enemyBullet_b = enemyBullets_4.getFirstExists(false);
    enemyBullet_c = enemyBullets_5.getFirstExists(false);
    livingEnemies.length=0;

    elite.forEachAlive(function(elite_m){

        // put every living enemy in an array
        livingEnemies.push(elite_m);
    });


    if (enemyBullet_a && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet_a.reset(shooter.body.x, shooter.body.y);
        enemyBullet_b.reset(shooter.body.x, shooter.body.y);
        enemyBullet_c.reset(shooter.body.x, shooter.body.y);
        
        enemyBullet_a.body.velocity.y = 450;
        enemyBullet_a.body.velocity.x = 300;
        
        enemyBullet_b.body.velocity.y = 450;

        enemyBullet_c.body.velocity.y = 450;
        enemyBullet_c.body.velocity.x = -300;
        ///game.physics.arcade.moveToObject(enemyBullet_a,player,120);
        firingTimer = game.time.now + 3000;
    }

}
function enemyFires_1(){

    //audio
    sword.play();
    enemyBullet_a = enemyBullets_1.getFirstExists(false);
    enemyBullet_b = enemyBullets_2.getFirstExists(false);
    livingEnemies.length=0;

    elite.forEachAlive(function(elite_m){

        // put every living enemy in an array
        livingEnemies.push(elite_m);
    });


    if (enemyBullet_a && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        enemyBullet_a.reset(shooter.body.x, shooter.body.y);
        enemyBullet_b.reset(shooter.body.x, shooter.body.y);
        game.physics.arcade.moveToObject(enemyBullet_a,player,800);
        game.physics.arcade.moveToObject(enemyBullet_b,player,550);
        firingTimer_1 = game.time.now + 5000;
    }
}
function suicideFires(){

    suicideEnemy = aliens_2.getFirstExists(false);
    livingEnemies.length=0;

    elite.forEachAlive(function(elite_m){

        // put every living enemy in an array
        livingEnemies.push(elite_m);
    });
    if (suicideEnemy && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        suicideEnemy.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(suicideEnemy,player,120);
        firingTimer_2 = game.time.now + 3000;
    }
}
function suicideFires_1(){

    suicideEnemy = aliens_3.getFirstExists(false);
    livingEnemies.length=0;

    elite.forEachAlive(function(elite_m){

        // put every living enemy in an array
        livingEnemies.push(elite_m);
    });
    if (suicideEnemy && livingEnemies.length > 0)
    {
        
        var random=game.rnd.integerInRange(0,livingEnemies.length-1);

        // randomly select one of them
        var shooter=livingEnemies[random];
        // And fire the bullet from this enemy
        suicideEnemy.reset(shooter.body.x, shooter.body.y);

        game.physics.arcade.moveToObject(suicideEnemy,player,300);
        firingTimer_3 = game.time.now + 5000;
    }
}
function fireBullet () {

    //  To avoid them being allowed to fire too fast we set a time limit
    if (game.time.now > bulletTime)
    {
        //  Grab the first bullet we can from the pool
        bullet = bullets.getFirstExists(false);

        if (bullet)
        {
            //  And fire it
            bullet.reset(player.x, player.y + 8);
            bullet.body.velocity.y = -400;
            bulletTime = game.time.now + 700;
        }
    }

}
function fireBullet_1(){

    if (game.time.now > bulletTime_1)
    {
        //  Grab the first bullet we can from the pool
        skill_state_1 = 'Reloading';
        skillText.text = skillString_1 + skill_state_1;
        bullet_a = bullets_1.getFirstExists(false);
        bullet_b = bullets_2.getFirstExists(false);
        bullet_c = bullets_3.getFirstExists(false);
        if (bullet_a)
        {
            //  And fire it
            bullet_a.reset(player.x, player.y + 12);
            bullet_a.body.velocity.y = -700;
            bullet_b.reset(player.x, player.y + 10);
            bullet_b.body.velocity.y = -500;
            bullet_c.reset(player.x, player.y + 8);
            bullet_c.body.velocity.y = -300;
            bulletTime_1 = game.time.now + 8000;
        }
    }
}
function fireBullet_2(){

    if (game.time.now > bulletTime_2)
    {
        //  Grab the first bullet we can from the pool
        skill_state_2 = 'Reloading';
        skillText_0.text = skillString_2 + skill_state_2;
        bullet_a = bullets_1.getFirstExists(false);
        bullet_b = bullets_2.getFirstExists(false);
        bullet_c = bullets_3.getFirstExists(false);
        bullet_e = bullets_4.getFirstExists(false);
        bullet_f = bullets_5.getFirstExists(false);
        bullet_g = bullets_6.getFirstExists(false);
        if (bullet_a)
        {
            //  And fire it
            bullet_e.reset(player.x, player.y + 8);
            bullet_e.body.velocity.y = -600;
            bullet_e.body.velocity.x = -500;

            bullet_a.reset(player.x, player.y + 8);
            bullet_a.body.velocity.y = -500;
            bullet_a.body.velocity.x = -350;

            bullet_f.reset(player.x, player.y + 8);
            bullet_f.body.velocity.y = -650;
            ///bullet_f.body.velocity.x = -300;

            bullet_c.reset(player.x, player.y + 8);
            bullet_c.body.velocity.y = -400;
            ///bullet_c.body.velocity.x = 300;

            bullet_b.reset(player.x, player.y + 8);
            bullet_b.body.velocity.y = -500;
            bullet_b.body.velocity.x = 350;

            bullet_g.reset(player.x, player.y + 8);
            bullet_g.body.velocity.y = -600;
            bullet_g.body.velocity.x = 500;

            bulletTime_2 = game.time.now + 12000;
        }
    }
}
/*function pauseAct(){

    if(game.paused == true){

        game.paused == false;
        pauseing.visible = false;
    }
    else{

        game.paused == true;
        pauseing.visible = false;
    }
}*/
function resetBullet (bullet) {

    //  Called if the bullet goes out of the screen
    bullet.kill();

}

function restart () {

    //  A new level starts
    /*cur_live = 5;
    //resets the life count
    lives.callAll('revive');
    elite_lives.callAll('revive');
    guards_1_lives.callAll('revive');
    guards_2_lives.callAll('revive');
    guards_3_lives.callAll('revive');
    //  And brings the aliens back from the dead :)
    aliens.removeAll();
    
    createAliens();

    //revives the player
    player.revive();
    //hides the text
    stateText.visible = false;
    score = 0;
    scoreText.text = scoreString + score;*/
    create();
}
